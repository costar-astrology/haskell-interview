# haskell-interview

### Background

This repo is intended to be a base for a technical interview.

There is a small REST API skeleton set up using [persistent](http://hackage.haskell.org/package/persistent-2.10.0) and [servant](http://hackage.haskell.org/package/servant). There are two
endpoints described in the API (but not currently implemented):

- GET /user
  - Return a list of users in the database. A user has a username and birthdate,
    but when we return users from this endpoint, we don't want to disclose the
    birthdates. Instead we want to transform that information into their sun sign.
    
    There's more information about calculating a sun sign [here](https://en.wikipedia.org/wiki/Astrological_sign#Dates_table)
- POST /user
  - Create a new user, persisting their info to the database.


Persistent is set up to use an in-memory sqlite database. It shouldn't require 
any configuration, but you may need to install sqlite if you don't have it already.

Since the database is in-memory, any info will be lost when the program exits. 

### Goals

1. Implement the missing endpoints. 
2. Write some tests
3. Extend the database model + API to support friends with the following in mind:
   - It should be possible to add someone as a friend
   - It should be possible to get a user's list of friends
   - A friendship must be mutual between two people to show up in a friend list 

### To run

- `> stack build`
- `> stack exec haskell-interview-exe`

```
> curl localhost:8080/user -i
HTTP/1.1 500 Internal Server Error
Transfer-Encoding: chunked
Date: Mon, 15 Apr 2019 22:04:54 GMT
Server: Warp/3.2.26
Content-Type: text/plain; charset=utf-8

Something went wrong%
```