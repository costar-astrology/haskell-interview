module Handlers where

import           API
import           Control.Monad.Except
import           Control.Monad.Reader
import           Database.Persist.Sql
import           Models
import           Servant
import           Servant.API.Generic
import           Servant.Server.Generic

type AppT = ReaderT SqlBackend (ExceptT ServantErr IO)

baseApi :: ToServant BaseApi (AsServerT AppT)
baseApi = genericServerT BaseApi
  { user = userApi
  }

userApi :: ToServant UserApi (AsServerT AppT)
userApi = genericServerT UserApi
  { create = undefined
  , list = undefined
  }
