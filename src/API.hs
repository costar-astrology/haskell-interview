{-# LANGUAGE DataKinds      #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric  #-}
{-# LANGUAGE TypeOperators  #-}

module API where

import           Data.Aeson
import           Models
import           Servant
import           Servant.API.Generic

data UserCreate = UserCreate
  { -- add fields
  } deriving (Generic, FromJSON)

data UserResponse = UserResponse
  { -- add fields
  } deriving (Generic, ToJSON)


data UserApi route = UserApi
  { create :: route :- ReqBody '[JSON] UserCreate :> Post '[JSON] ()
  , list   :: route :- Get '[JSON] [UserResponse]
  } deriving (Generic)

data BaseApi route = BaseApi
  { user :: route :- "user" :> ToServantApi UserApi
  } deriving (Generic)

type Api = ToServantApi BaseApi

