{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeApplications  #-}

module Main where

import           API
import           Handlers
import           Models

import           Control.Monad.Except
import           Control.Monad.Logger
import           Control.Monad.Reader
import           Control.Monad.Trans.Class
import           Database.Persist.Sqlite
import           Network.Wai
import           Network.Wai.Handler.Warp  (run)
import           Servant

main :: IO ()
main = runStdoutLoggingT . withSqliteConn ":memory:" $ \sqlite -> do
  flip runSqlConn sqlite $ runMigration migrateAll
  let apiProxy = Proxy @Api
  lift . run 8080 . serve apiProxy $ hoistServer apiProxy (runHandler sqlite) baseApi

  where
    runHandler sqlite = Handler . flip runReaderT sqlite

